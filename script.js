function getEpoch() {document.getElementById("getEpochOutput").innerHTML = parseInt((new Date().getTime() / 1000).toFixed(0))}

function epochToTime() {
 var timestamp = new Date(document.getElementById("epochField").value*1000);
 timestamp.setMinutes(timestamp.getMinutes() - timestamp.getTimezoneOffset());
 document.getElementById("timeField").value = timestamp.toISOString().slice(0,16);
}

function timeToEpoch() {document.getElementById("epochField").value = new Date(document.getElementById("timeField").value).getTime()/1000.0;}

function TVM() {
  document.getElementById("epochField").value = Math.trunc(new Date().getTime()/1000.0);
  epochToTime();
}

epochToTime();
setInterval(getEpoch, 1000);
